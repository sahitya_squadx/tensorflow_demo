First of all we need to install Tensor flow in our system 

  Use follwoing command to install tensro flow 
  
  pip install tensorflow
  
There is alreay flower folder in this project if not then use following command in terminal to download all flower folder (Before running this command be sure you are in the folder of this project with terminal)

  curl http://download.tensorflow.org/example_images/flower_photos.tgz | tar xz -C tf_files
  
Run following script in the directory to train the module 

 IMAGE_SIZE=224 ARCHITECTURE="mobilenet_0.50_224" python -m scripts.retrain \  --flower_dir=tf_files/flower_photos \  --model_dir=tf_files/models/mobilenet_0.50_224 \  --summaries_dir=tf_files/training_summaries/mobilenet_0.50_224 \  --output_graph=tf_files/retrained_graph.pb \  --output_labels=tf_files/retrained_labels.txt \  --architecture=mobilenet_0.50_224 \  --image_dir=./tf_files/flower_photo
 
This will train for all type of flowers that is there in tf_files/flower_photos directory

Now you can check any other image of flower to run your test case, To check if the selected flower comes in this category run following command

python -m scripts.label_image \    --graph=tf_files/retrained_graph.pb  \    --image=PATH_TO_IMAGE_HERE


provide complete path of the image from root  
  